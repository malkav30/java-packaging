package isen.quiz.view;

import isen.quiz.App;
import javafx.fxml.FXML;

public class HomeScreenController {

	@FXML
	public void handleLaunchButton() throws Exception {
		App.showView("QuizOverview");
	}

}
