package isen.quiz.view;

import java.io.IOException;

import isen.quiz.App;
import javafx.application.Platform;

public class MainLayoutController {

	public void closeApplication() {
		Platform.exit();
	}

	/**
	 * Method allowing us to go back to the home screen
	 * @throws IOException 
	 */
	public void gotoHome() throws IOException {
		App.showView("HomeScreen");
	}

	/**
	 * Method allowing us to go to the Questions Administration view
	 * @throws IOException 
	 */
	public void gotoQuestionAdmin() throws IOException {
		// OK, maybe six lines were too much...
		App.showView("QuestionsAdmin");
	}

}
