package isen.quiz.view;

import isen.quiz.model.Question;
import isen.quiz.service.QuestionService;
import isen.quiz.util.QuestionValueFactory;
import isen.quiz.util.QuizChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 * @author A501341
 *
 */
public class QuestionsAdminController {

	@FXML
	TableView<Question> questionsTable;

	@FXML
	TableColumn<Question, String> questionColumn;

	@FXML
	AnchorPane formPane;

	@FXML
	TextField questionField;

	@FXML
	TextField answer1Field;

	@FXML
	TextField answer2Field;

	@FXML
	TextField answer3Field;

	@FXML
	RadioButton radio1;

	@FXML
	RadioButton radio2;

	@FXML
	RadioButton radio3;

	Question currentQuestion;

	@FXML
	private void handleSaveButton() {
		// Its implementation is pretty simple because it gets the content
		// of each field in order to fill the information stored inside currentQuestion.
		// Once it’s done, it resets the view. But beware, IT'S A TRAP !
		// At this point, maybe you tried to create a new Question object, to, you know,
		// "keep it compact". this gives you the following code :

//		this.currentQuestion = new Question(this.questionField.getText(), new Answer(answer1Field.getText(), radio1.isSelected()),
//				new Answer(answer2Field.getText(), radio2.isSelected()),
//				new Answer(answer3Field.getText(), radio3.isSelected()));

		// but strangely enough, it does not work ! Damn, what's wrong ?
		// pretty simple in fact : you are working with a new local pointer, which will
		// replace the original question of the model. Once the method returns, the
		// currentQestion still holds your new question, but we didn't modify the
		// question of the list by doing this !

		// This is the way to go :
		this.currentQuestion.setQuestion(this.questionField.getText());
		this.currentQuestion.getAnswer1().setGoodAnswer(radio1.isSelected());
		this.currentQuestion.getAnswer1().setText(answer1Field.getText());
		this.currentQuestion.getAnswer2().setGoodAnswer(radio2.isSelected());
		this.currentQuestion.getAnswer2().setText(answer2Field.getText());
		this.currentQuestion.getAnswer3().setGoodAnswer(radio3.isSelected());
		this.currentQuestion.getAnswer3().setText(answer3Field.getText());

		this.resetView();
	}

	/**
	 * This method handles tthe creation oof a new question
	 * 1.	It creates a new Question object (a constructor is provided with default values)
	 * 2.	It add this question to the list of questions (use QuestionService)
	 * 3.	It selects this new question in the list : questionsTable.getSelectionModel().select(newQuestion);
 	 */
	@FXML
	private void handleNewButton() {
		Question question = new Question();
		QuestionService.addQuestion(question);
		this.questionsTable.getSelectionModel().select(question);
	}

	/**
	 * this method handles the deletion of an existing question. Just copy/paste
	 */
	@FXML
	private void handleDeleteButton() {
		int selectedIndex = this.questionsTable.getSelectionModel().getSelectedIndex();
	    if (selectedIndex >= 0) {
	        questionsTable.getItems().remove(selectedIndex);
	        resetView();
	    }
	}

	/**
	 * refreshes the questionsTable (take a look at its available methods) and
	 * clears its selection model.
	 */
	private void refreshList() {
		this.questionsTable.refresh();
		this.questionsTable.getSelectionModel().clearSelection();
	}

	/**
	 * sets the items of questionsTable (QuestionService will be helpful) then
	 * refreshes the list
	 */
	private void populateList() {
		this.questionsTable.setItems(QuestionService.getQuestions());
		this.questionsTable.refresh();
	}

	/**
	 * Initializes the editor : 1. It calls the setCellValueFactory method of
	 * questionColumn. The parameter of this method is a new instance of
	 * QuestionValueFactory. (If you are curious and look what this class does, it
	 * will print the text of the question in the column) 2. It populates the list
	 */
	@FXML
	private void initialize() {
		this.questionColumn.setCellValueFactory(new QuestionValueFactory());
		this.populateList();
		this.questionsTable.getSelectionModel().selectedItemProperty().addListener(new QuizChangeListener() {
			// here we use the QuizChangeListener. It's value is only to avoid you the boilerplate code
			// of the original ChangleListener Method 
			@Override
			public void handleNewValue(Question newValue) {
				showQuestionDetails(newValue);
				
			}
		});
		this.resetView();
	}

	/**
	 * 1. If the parameter is null, it hides the formPane by calling its setVisible
	 * method. 
	 * 2. If the parameter is not null:
	 *   a. It show the formPane by callingits setVisible method 
	 *   b. It sets currentQuestion with the object passed as parameter. 
	 *   c. It calls the setText method of questionField, answer1Field,
	 *      answer2Field and answer3Field in order to fill these fields with the
	 *      attributes of currentQuestion. 
	 *   d. It calls setSelected method on the 3 radio with the value returned by 
	 *      the isGoodAnswer method of each answer of currentQuestion
	 * 
	 */
	private void showQuestionDetails(Question question) {
		if (question == null) {
			this.formPane.setVisible(false);
		} else {
			this.formPane.setVisible(true);
			this.currentQuestion = question;
			this.questionField.setText(currentQuestion.getQuestion());
			this.answer1Field.setText(currentQuestion.getAnswer1().getText());
			this.answer2Field.setText(currentQuestion.getAnswer2().getText());
			this.answer3Field.setText(currentQuestion.getAnswer3().getText());
			this.radio1.setSelected(currentQuestion.getAnswer1().isGoodAnswer());
			this.radio2.setSelected(currentQuestion.getAnswer2().isGoodAnswer());
			this.radio3.setSelected(currentQuestion.getAnswer3().isGoodAnswer());
		}
	}

	/**
	 * Nothing fancy here, this is just a way to reset our actual view with an non visible form
	 */
	private void resetView() {
		this.showQuestionDetails(null);
		this.refreshList();
	}
}
